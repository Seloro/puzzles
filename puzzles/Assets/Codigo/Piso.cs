using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piso : MonoBehaviour
{
    public int indise;
    static public int indicePublico;
    public bool finMision, noCambioDeColor;
    Material material;
    private void Start()
    {
        material = GetComponent<MeshRenderer>().material;
        indicePublico = -1;
    }
    private void OnCollisionEnter(Collision collision)// cuando el piso entra encontacto con el jugador, si no es una meta se canbia a verde. Ademas cambia el indise publico que se usa en UI
    {
        if (collision.gameObject.CompareTag("Player") && finMision)
        {
            indicePublico = indise;
        }
        if (!noCambioDeColor)
            material.color = Color.green;
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && finMision)
        {
            indicePublico = -1;
        }
    }
}
