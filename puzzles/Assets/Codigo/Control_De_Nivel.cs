using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Control_De_Nivel : MonoBehaviour
{
    static public int nivel;
    static public bool creado;
    private void Start()
    {
        if (!creado)// destrulle o evita que se destrulla al cambiar de esena el objeto
        {
            DontDestroyOnLoad(gameObject);
            creado = true;
        }
        else
            Destroy(gameObject);
        nivel = 0;
    }
    void Update()
    {
        reinicio();
        avansarNivel();
        trampa();
    }
    private void reinicio()// reacarga la esena al presionar R
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(nivel);
        }
    }
    void avansarNivel()// si las condisiones de victoria se pumple camvia a la esena siguiente tras apretar enter
    {
        if (GameManager.gano)
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                nivel++;
                SceneManager.LoadScene(nivel);
            }
        }
    }
    void trampa()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            nivel++;
            SceneManager.LoadScene(nivel);
            Time.timeScale = 1;
        }
    }
}
