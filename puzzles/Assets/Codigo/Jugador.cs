using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    static public bool enPiso = true;
    private void OnCollisionEnter(Collision collision)// abilita el movimiento cuando colisiona con algo (solo hay piso en el juego)
    {
        enPiso = true;
    }
    private void OnCollisionExit(Collision collision)// desabilita el movimiento cuando deja de estar en colicion con algo
    {
        enPiso = false;
    }
}
