using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public List<string> info;
    public Text text, ganar;
    int indise;
    public List<bool> completo;
    private void Start()
    {
        recargar();
        ganar.gameObject.SetActive(false);
    }
    private void Update()
    {
        inPuts();
        recargar();
        comprobacion();
        if (GameManager.gano)
            ganar.gameObject.SetActive(true);
    }
    void inPuts()// cambia el indise que muestra el texto
    {
        if (Input.GetKeyDown(KeyCode.Q))
            indise -= 1;
        else if (Input.GetKeyDown(KeyCode.E))
            indise += 1;
        if (indise >= info.Count)
            indise = 0;
        else if (indise < 0)
            indise = info.Count - 1;
    }
    void recargar()
    {
        text.text = info[indise];// cambia el texto a uno de la lista de string segun el indice actual
        if (completo[indise])// si el indise actual tiene su corespondiente booleano verdadero se muestra con letras verdes sino con negras
            text.color = Color.green;
        else
            text.color = Color.black;
    }
    void comprobacion()
    {
        if (Piso.indicePublico >= 0)// si el indise de Piso es mayor o igual a 1, ace verdadero el booleano en dichio indise
            completo[Piso.indicePublico] = true;
    }
}
