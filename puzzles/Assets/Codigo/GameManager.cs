using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class GameManager : MonoBehaviour
{
    public GameObject jugador, inicio, meta;
    public List<GameObject> piso;
    public int x, y;
    bool recargar;
    static public bool gano;
    int xInicial, yInicial;
    private void Start()
    {
        repocicionamiento();
        gano = false;
        xInicial = x;
        yInicial = y;
    }
    private void Update()
    {
        inPuts();
        repocicionamiento();
        comprovacionDeGanar();
    }
    void inPuts()
    {
        if (Jugador.enPiso && !gano)// mueve las variables X e Y que seutilisan para saber a que posicion espesifica de una matris mover al jugador
        {
            if (Input.GetKeyDown(KeyCode.W) && y < 9)
            {
                y += 1;
                recargar = true;
            }
            else if (Input.GetKeyDown(KeyCode.S) && y > 0)
            {
                y -= 1;
                recargar = true;
            }
            if (Input.GetKeyDown(KeyCode.D) && x < 9)
            {
                x += 1;
                recargar = true;
            }
            else if (Input.GetKeyDown(KeyCode.A) && x > 0)
            {
                x -= 1;
                recargar = true;
            }
        }
    }
    void repocicionamiento()
    {
        if (recargar)// repocisiona al jugador a la ubicasion perteneciente a uno de los bloques de piso
        {
            jugador.transform.position = piso[y].transform.GetChild(x).position + (Vector3.up * 1.5f);
            recargar = false;
        }
        if (jugador.transform.position.y < -5)// repociciona al jugador en el punto de inicio si este se cae
        {
            jugador.transform.position = inicio.transform.position + (Vector3.up * 1.5f);
            x = xInicial;
            y = yInicial;
        }
    }
    void comprovacionDeGanar()// el jugador gana si esta en las mismas cordenadas X y Z de la meta
    {
        if (jugador.transform.position.x == meta.transform.position.x && jugador.transform.position.z == meta.transform.position.z)
            gano = true;
    }
}
